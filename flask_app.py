__author__ = 'Oskar Vuola'
from flask import Flask, redirect, request, abort
import uuid

"""Initialize the flask app"""
app = Flask(__name__)

"""Initialize data base; type=dict"""
database = {}

""" Route for redirecting user based on the shortened id.
    If there is no id or the id is not found in the database, return 404.
    If there is such id in database, redirect to stored URL.
"""
@app.route('/<id>', methods=['GET'])
def shortlink_redirect(id=None):
    str_id = str(id)
    if str_id is None:
        abort(404)
    else:
        url = database.get(str_id)
        if url is not None:
            return (redirect(url), 301)
        else:
            abort(404)

"""Route to shorten links using POST
    Takes the URL to be shortened from
    Content-Type: application/x-www-form-urlencoded form
    and assigns it an id (truncated UUID). If the id already
    exists in the database, UUID is regenerated until such UUID
    is found that doesn't already exist in the database.
    Returns: ID for the url
"""
@app.route('/shorten', methods=['POST'])
def create_shortlink():
    link_to_shorten = request.form['link']
    if len(link_to_shorten) is 0 or len(link_to_shorten) > 10000:
        abort(400)
    id = str(uuid.uuid4().get_hex().upper()[0:6])
    while id in database.keys():
        id = str(uuid.uuid4().get_hex().upper()[0:6])
    database[id] = link_to_shorten
    return id


if __name__ == '__main__':
    """Run the app"""
    app.run()